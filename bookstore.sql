use bookstore;

CReate table `address`(
`addressId` varchar(30) not null primary key,
`client_post_index` varchar(30),
 `client_post_index_address` varchar(30),
 `client_post_index_phone` varchar(30),
 `client_phone` varchar(30)
);
Create table `author` (
`authorId` varchar(30) not null primary key,
`authorName` varchar(30) not null
);
Create table `client` (
`clientId` varchar(30) not null primary key,
`clientName` varchar(30) not null,
`addressId` varchar(30) references `address` (addressId)
);

Create table `book`(
`bookId` varchar(30) not null primary key,
`aurhorId` varchar(30) references `author` (authorId),
`published_year` date,
`book_price` double,
`book_couunt` int
);

Create table `sale`(
`saleId` varchar(30) not null primary key,
`bookId` varchar(30) references `book` (bookId),
`clientId` varchar(30) references `client` (clientId),
`saleDate` date,
`count` int
);

Create table `insertBook`(
`insertId` varchar(30) not null primary key,
`bookId` varchar(30)  references `book` (bookID),
`insertDate` date,
`insertCount` int
);